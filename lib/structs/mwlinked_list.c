#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct MWLinkedList {
    int x;
    struct MWLinkedList* next;
} MWLinkedList;

MWLinkedList* insertFirst(MWLinkedList* list, int x);
MWLinkedList* insertLast(MWLinkedList* list, int x);
MWLinkedList* removeFirst(MWLinkedList* list);
MWLinkedList* removeLast(MWLinkedList* list);
int length(MWLinkedList* list);
int elementAt(MWLinkedList* list, int index);
void setElementAt(MWLinkedList* list, int index, int x);
void printList(MWLinkedList* list);
void insertFirstRef(MWLinkedList** refList, int x);
void insertLastRef(MWLinkedList** list, int x);

int main(void) {
  MWLinkedList* list = NULL;
  printList(list); // [ ]

  list = insertFirst(list, 10);  // [ 10 ]
 //insertFirstRef(&list, 10);
  
  printList(list);

  list = insertFirst(list, 20); //[ 20, 10 ]
  printList(list);

  list = insertFirst(list, 30); //[30, 20, 10]
  printList(list);

  list = NULL;

  //list = insertLast(list, 10); // [10]
  insertLastRef(&list,10);
  printList(list);

  list = insertLast(list, 20); //[ 10, 20 ]
 //insertLastRef(&list,20);
  printList(list);


  list = insertLast(list, 30); // [10, 20, 30]
  printList(list);
  sleep(2);

  list = insertLast(list, 40); // [10, 20, 30, 40]
  printList(list);

  printf("length: %d\n",length(list)); // 4

  printf("%d\n",elementAt(list, 0)); // 10

  list = removeFirst(list); // [20,30,40]
  printList(list);

  printf("%d\n",elementAt(list, 0)); //20

  list = removeFirst(list); //[30,40]
  printList(list);

  printf("%d\n",elementAt(list, 1)); //40

  list = removeLast(list); //[30]
  printList(list);

  printf("%d\n",elementAt(list, 0)); //30

  printf("%d\n",length(list)); // 1

  list = removeLast(list); // []
  printList(list);

  printf("%d\n",length(list)); // 0

  list = insertLast(list, 10);
  list = insertLast(list, 11);
  list = insertLast(list, 12);
  list = insertLast(list, 13);
  list = insertLast(list, 14);

  printf("%d\n",elementAt(list, 1)); //11
  setElementAt(list, 1, 100);
  printf("%d\n",elementAt(list, 1)); //11

  int limit = 1024*100;
  puts("will insert");
  for(int i = 0; i < limit; i++){
    list = insertLast(list, i);
  }
  puts("finish insert");
  printf("%d\n",length(list)); //100 000 005


  return 0;
}



MWLinkedList* insertFirst(MWLinkedList* list, int x){
    MWLinkedList* newNode = (MWLinkedList*)malloc(sizeof(MWLinkedList));
    newNode->x = x;
    newNode->next = list;
    return newNode;
}

MWLinkedList* insertLast(MWLinkedList* list, int x) {
    MWLinkedList* newNode = (MWLinkedList*)malloc(sizeof(MWLinkedList));
    newNode->x = x;
    newNode->next = NULL;
    if (list == NULL) {
      return newNode;
    }
    MWLinkedList* p = list;
    while (p->next != NULL) {
      p = p->next;
    }
    p->next = newNode;
    return list;
 }

  MWLinkedList* removeFirst(MWLinkedList* list) {
    if (list == NULL) {
      printf("Erro! A lista já é vazia\n");
      return NULL;
    }
    MWLinkedList* newFirst = list->next;
    free(list);
    return newFirst;
  }

  MWLinkedList* removeLast(MWLinkedList* list) {
    if (list == NULL) {
      printf("Erro! A lista já é vazia\n");
      return NULL;
    }
    if (list->next == NULL) {
      free(list);
      return NULL; //Remover a si mesmo
    }
    MWLinkedList* p = list;
    MWLinkedList* q = p;
    while (p->next != NULL) {
      q = p;
      p = p->next;
    }
    free(p);
    q->next = NULL;
    return list;
  }

  int length(MWLinkedList* list) {
    int i = 0;
    MWLinkedList* p = list;
    while (p != NULL) {
      p = p->next;
      i++;
    }
    return i;
  }

   int elementAt(MWLinkedList* list, int index) {
    if (list == NULL) {
        printf("Erro! Lista vazia e vc tentou accessar o elemento [%d]\n",index);
        printf("fechando o programa pq vc eh um pau no cu!\n");
        exit(01);
       return -1;
    }
    int i = 0;
    MWLinkedList* p = list;
    while (p->next != NULL && i < index) {
      p = p->next;
      i++;
    }
    return p->x;
  }

 void setElementAt(MWLinkedList* list, int index, int x) {
    if (list == NULL) {
      return;
    }
    int i = 0;
    MWLinkedList* p = list;
    while (p->next != NULL && i < index) {
      p = p->next;
      i++;
    }
    p->x = x;
  }

 void printList(MWLinkedList* list){
    MWLinkedList* p = list;
    printf("[");
    while(p){
        printf( p->next ? "%d, " : "%d",p->x);
        p = p->next;
    }
    printf("]\n");
 }

void insertFirstRef(MWLinkedList** refList, int x){
    MWLinkedList* newNode = (MWLinkedList*)malloc(sizeof(MWLinkedList));
    newNode->x = x;
    newNode->next = *refList;
    *refList = newNode;
}
void insertLastRef(MWLinkedList** list, int x){
    MWLinkedList* newNode = (MWLinkedList*)malloc(sizeof(MWLinkedList));
    newNode->x = x;
    newNode->next = NULL;
    if (*list == NULL) {
      *list = newNode;
      return;
    }
    MWLinkedList* p = *list;
    while (p->next != NULL) {
      p = p->next;
    }
    p->next = newNode;
    
}
 void removeFirstRef(MWLinkedList** list) {
    if (*list == NULL) {
      printf("Erro! A lista já é vazia\n");
      return;
    }
    MWLinkedList* newFirst = (*list)->next;
    free(*list);
    *list = newFirst;
}

 void removeLastRef(MWLinkedList** list) {
    if (*list == NULL) {
      printf("Erro! A lista já é vazia\n");
      return;
    }
    if ((*list)->next == NULL) {
      free(*list);  //Remover a si mesmo
      return;
    }
    MWLinkedList* p = *list;
    MWLinkedList* q = p;
    while (p->next != NULL) {
      q = p;
      p = p->next;
    }
    free(p);
    q->next = NULL;
  }


