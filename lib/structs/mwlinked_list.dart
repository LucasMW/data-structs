class MWLinkedList {
  late int x;
  MWLinkedList? next;

  static MWLinkedList insertFirst(MWLinkedList? list, int x) {
    final newNode = MWLinkedList();
    newNode.x = x;
    newNode.next = list;
    // print("newNode($x,$list)");
    return newNode;
  }

  static MWLinkedList insertLast(MWLinkedList? list, int x) {
    final newNode = MWLinkedList();
    newNode.x = x;
    newNode.next = null;
    if (list == null) {
      return newNode;
    }
    MWLinkedList? p = list;
    while (p?.next != null) {
      p = p?.next;
    }
    p?.next = newNode;
    return list;
  }

  static MWLinkedList? removeFirst(MWLinkedList? list) {
    if (list == null) {
      //print("Erro! A lista já é vazia");
      return null;
    }
    final newFirst = list.next;
    list = null; // libera a memória deste nó.
    return newFirst;
  }

  static MWLinkedList? removeLast(MWLinkedList? list) {
    if (list == null) {
      print("Erro! A lista já é vazia");
      return null;
    }
    if (list.next == null) {
      return null; //Remover a si mesmo
    }
    MWLinkedList? p = list;
    MWLinkedList? q = p;
    while (p?.next != null) {
      q = p;
      p = p?.next;
    }
    q?.next = null;
    return list;
  }

  static int? elementAt(MWLinkedList? list, int index) {
    if (list == null) {
      return null;
    }
    int i = 0;
    MWLinkedList? p = list;
    while (p?.next != null && i < index) {
      p = p?.next;
      i++;
    }
    return p?.x;
  }

  static void setElementAt(MWLinkedList? list, int index, int x) {
    if (list == null) {
      return;
    }
    int i = 0;
    MWLinkedList? p = list;
    while (p?.next != null && i < index) {
      p = p?.next;
      i++;
    }
    p?.x = x;
  }

  static MWLinkedList? nodeAt(MWLinkedList? list, int index) {
    if (list == null) {
      return null;
    }
    int i = 0;
    MWLinkedList? p = list;
    while (p?.next != null && i < index) {
      p = p?.next;
      i++;
    }
    return p;
  }

  bool isLast(int index) {
    return nodeAt(this, index)?.next == null;
  }

  int operator [](int index) {
    return MWLinkedList.elementAt(this, index) ?? 0;
  }

  void operator []=(int index, int value) {
    MWLinkedList.setElementAt(this, index, value);
  }

  static int length(MWLinkedList? list) {
    int i = 0;
    MWLinkedList? p = list;
    while (p != null) {
      p = p.next;
      i++;
    }
    return i;
  }

  int get count {
    return length(this);
  }

  @override
  String toString() {
    String x = "[";
    MWLinkedList? p = this;
    while (p != null) {
      x = "$x ${p.x},";
      p = p.next;
    }
    x = x.substring(0, x.length > 2 ? x.length - 1 : x.length);
    x = "$x ]";
    return x.replaceAll(" ", "");
  }

  static void printList(MWLinkedList? list) {
    String x = "[";
    MWLinkedList? p = list;
    while (p != null) {
      x = "$x ${p.x},";
      p = p.next;
    }
    x = x.substring(0, x.length > 2 ? x.length - 1 : x.length);
    x = "$x ]";
    //x = x.replaceAll(" ", "");
    print(x);
  }
}
