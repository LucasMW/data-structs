import 'package:data_structs/structs/mwlinked_list.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  MWLinkedList? list;
  int _currentOperation = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            list == null
                ? Text("List is Empty!")
                : Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: 100,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: list!.count,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            color: Colors.primaries.first.shade100,
                            width: 200,
                            height: 50,
                            child: Center(
                              child: ListTile(
                                trailing: !list!.isLast(index)
                                    ? Icon(Icons.arrow_right_alt)
                                    : Container(
                                        width: 70,
                                        child: Row(
                                          children: [
                                            Icon(Icons.arrow_right_alt),
                                            Container(
                                                color: Colors.black,
                                                child: Text(
                                                  "NULL",
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ))
                                          ],
                                        ),
                                      ),
                                leading: Container(
                                  width: 100,
                                  height: 70,
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.black)),
                                  child: Center(
                                    child: Text(
                                      list![index].toString(),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          );
                        }),
                  ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      ElevatedButton(
                          onPressed: () {
                            setState(() {
                              list = MWLinkedList.insertFirst(
                                  list, _currentOperation++);
                            });
                          },
                          child: Text("Add First")),
                      Expanded(child: Container()),
                      ElevatedButton(
                          onPressed: () {
                            setState(() {
                              list = MWLinkedList.insertLast(
                                  list, _currentOperation++);
                            });
                          },
                          child: Text("Add Last"))
                    ],
                  ),
                  Row(
                    children: [
                      ElevatedButton(
                          onPressed: () {
                            if (list == null) {
                              print("vai tomar no cu!");
                              return;
                            }
                            setState(() {
                              list = MWLinkedList.removeFirst(list);
                            });
                          },
                          child: Text("Remove First")),
                      Expanded(child: Container()),
                      ElevatedButton(
                          onPressed: () {
                            if (list == null) {
                              print("vai tomar no cu!");
                              return;
                            }
                            setState(() {
                              list = MWLinkedList.removeLast(list);
                            });
                          },
                          child: Text("Remove Last"))
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      // floatingActionButton: FloatingActionButton.large(
      //   onPressed: () {
      //     setState(() {
      //       list = MWLinkedList.insertFirst(list, _currentOperation++);
      //     });
      //   },
      //   child: Icon(Icons.add),
      // ),
    );
  }
}
